# Node JS Application Source-to-Image

Example application to show how NodeJS source-to-image works.

## Deploy it on Openshift
    oc new-app  https://gitlab.com/openshift-samples/node-example.git -l template=node-example

## Clean Created Objects
    oc delete all -l template=node-example
