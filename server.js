var http = require('http');

http.createServer(function (req, res) {
    
   var msg = `${process.env.MESSAGE}`;
    
   if(msg =='undefined'){
       msg = ""
   }    
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end(`Hello from: ${process.env.HOSTNAME} !!!\n${msg}`);

}).listen(8080, '0.0.0.0');

console.log('Server running ');
